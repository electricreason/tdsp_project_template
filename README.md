 # TDSP Project Structure, and Documents and Artifact Templates

This is a modified version of the general project directory structure for Team Data Science Process developed by Microsoft. It also contains templates for various documents that are recommended as part of executing a data science project when using TDSP. The original directory structure has been modified by Rob McInerney.

[Team Data Science Process (TDSP)](https://docs.microsoft.com/en-us/azure/machine-learning/team-data-science-process/overview) is an agile, iterative, data science methodology to improve collaboration and team learning. It is supported through a lifecycle definition, standard project structure, artifact templates, and [tools](https://github.com/Azure/Azure-TDSP-Utilities) for productive data science.


**NOTE:** In this directory structure, the **Sample_Data folder is NOT supposed to contain LARGE raw or processed data**. It is only supposed to contain **small and sample** data sets, which could be used to test the code.

The two documents under Docs/Project, namely the [Charter](./docs/project/charter.md) and [Exit Report](./docs/project/exit%20report.md) are particularly important to consider. They help to define the project at the start of an engagement, and provide a final report to the customer or client.

**NOTE:** In some projects, e.g. short term proof of principle (PoC) or proof of value (PoV) engagements, it can be relatively time consuming to create and all the recommended documents and artifacts. In that case, at least the Charter and Exit Report should be created and delivered to the customer or client. As necessary, organizations may modify certain sections of the documents. But it is strongly recommended that the content of the documents be maintained, as they provide important information about the project and deliverables.


Project Organization
------------

    ├── docs/                    <- Folder for hosting all documents for a Data Science Project
    │   │
    │   ├── data_dictionaries/   <- Field-level description data of data files
    │   ├── data_report/         <- Documents describing results of data exploration
    │   ├── model/               <- Documents and reports related to modeling
    │   └── project/             <- Documents and reports related to Data Science Project
    │       ├── charter.md       <- Defines a Data Science Project
    │       └── exit_report.md.  <- Generated at the end of a Data Science Project
    │
    │
    ├── notebooks/               <- Jupyter notebooks
    │
    │
    ├── sample_data/             <- The original, immutable sample data dump
    │   │
    │   ├── raw/                 <- The original, immutable sample data dump
    │   ├── processed/           <- Intermediate sample data that has been transformed
    │   └── for_modeling/        <- The final, canonical sample data sets for modeling
    │
    │
    ├── src/                     <- Source code for use in this project
    │   │
    │   ├── data/                <- Scripts for data acquisition and understanding (exploratory analysis)
    │   │   ├── data_pipeline.json
    │   │   └── data_prep.py
    │   │
    │   ├── models/              <- Scripts for modeling and related activities (such as feature
    │   │   │                      engineering, model evaluation etc.)
    │   │   └── model.py
    │   │
    │   └── deployment/          <- Scrips for model deployment
    │       └── operalization.py
    │
    │
    ├── testing/                 <- Files necessary for running model tests
    │
    │
    ├── LICENSE-CODE.txt
    ├── LICENSE.txt
    ├── NOTICE.txt
    ├── README.md               <- The top-level README for developers using this Data Science Project.
    └── requirements.txt        <- The requirements file for reproducing the analysis environment, e.g.
                                   generated with `pip freeze > requirements.txt`

--------
